import { Component,OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ApiProvider } from '../../../app/provider/api';

import { Injectable } from '@angular/core';
import { getLocaleDateFormat } from '@angular/common';
import { callbackify } from 'util';
import { AccountApi } from '../../../app/provider/accounts';


@Component({
  selector: 'app-facilities',
  templateUrl: './facilities.component.html',
  styleUrls: ['./facilities.component.css']
})
export class facilitiesComponent implements OnInit{
  
  playerData;
  accounts:AccountApi;


  constructor(
    public http:HttpClient
  ){
   this.accounts = new AccountApi(http);
  }


  ngOnInit(){

    this.accounts.getAccountData((res) => {
 
      this.playerData = res;
      console.log("log_- "+this.playerData)


    });
      } }