import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppRouting } from './app.routing';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { overviewComponent } from './home/overview/overview.component';
import {HttpClientModule} from '@angular/common/http';

@NgModule({

  declarations: [

    AppComponent,
    HomeComponent,
    overviewComponent
  ],
  imports: [
    
    BrowserModule,
    AppRoutingModule,
    AppRouting,
    HttpClientModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
