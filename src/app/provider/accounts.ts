import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { ApiProvider } from './api';



@Injectable()
export class AccountApi {
 
 constructor(public http: HttpClient) {}

 api: ApiProvider = new ApiProvider(this.http);

 getAccountData (callback){
  
    this.http.get(this.api.dataURL+'/account/start?idGoogle=2').subscribe(
        (response) => callback(response)
      
    );
  }



}

