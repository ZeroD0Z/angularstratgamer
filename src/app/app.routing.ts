import{Routes, RouterModule} from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { overviewComponent } from './home/overview/overview.component';
import { resourcesComponent } from './home/resources/resources.component';
import { loginComponent } from './login/login.component';
import { facilitiesComponent } from './home/facilities/facilities.component';

const routes: Routes = [

   {
      path: '',
      component: loginComponent,
   },

    {
        path: 'home',
        component: HomeComponent,
        children:[
   
            {
                path: 'overview',
                component: overviewComponent
             },
             {
                path: 'resources',
                component: resourcesComponent
             },
             {
                path: 'facilities',
                component: facilitiesComponent
             },
             {
                path: 'research',
                component: overviewComponent
             },
             {
                path: 'fleet',
                component: overviewComponent
             },
             {
                path: 'defenses',
                component: overviewComponent
             },
             {
                path: 'universe',
                component: overviewComponent
             },
             {
               path: '',
               component: overviewComponent
            }
        ]

     },
     {
      path: '**',
      redirectTo: ''
   }
    
];

export const AppRouting = RouterModule.forRoot(routes);